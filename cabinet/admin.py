from django.contrib import admin
from .models import AdvUser


class AdvUserAdmin(admin.ModelAdmin):
    list_display = ['username', 'sex', 'is_activated', 'send_messages', 'date_joined']
    list_filter = ['username', 'sex', 'is_activated', 'send_messages']
    list_editable = ['is_activated', 'send_messages']


admin.site.register(AdvUser, AdvUserAdmin)
