from django import forms


class SendMessage(forms.Form):
    text_data = forms.CharField(label='Сообщение', widget=forms.Textarea)
