from django.urls import path
from . import views
from django.conf.urls import url
from .views import *


app_name = 'mainapp'


urlpatterns = [
    url(r'^$', index, name='index'),
    path('send_message/<int:pk>/', send_message, name='send_message'),

    path('ajax/reload_group/', views.reload_group, name='ajax_reload_group'),  # скролл на странице index

    # панель вверху (раньше была слева)
    path('ajax/reload_data_left_bar/', views.reload_data_left_bar, name='ajax_reload_data_left_bar'),

    # страница комментариев
    path('ajax/reload_comment_block/<int:pk>/', reload_comment_block, name='ajax_reload_comment-block'),

    path('site-info/', site_info, name='site_info'),
    path('contacts/', contacts, name='contacts'),
]
