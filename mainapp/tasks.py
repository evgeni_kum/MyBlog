import logging
from django.template.loader import render_to_string

from cabinet.models import AdvUser
from blogsite.celery import app
from blogsite.settings import ALLOWED_HOSTS


""" ================= !!! ПРИ ЛЮБЫХ ИЗМЕНЕНИЯХ В ЭТОМ ФАЙЛЕ, ПЕРЕЗАПУСКАЕМ SELERY !!! ================="""


@app.task
def send_new_comment_notification(pk, current_blog, current_url):
    author = AdvUser.objects.get(pk=pk)

    # logging.info(f"Имя блога: {current_blog}")      # -->> нужно передать название блога
    # comment.entry_id.pk               -->> нужно передать pk сообщения для и спользовния в url

    if ALLOWED_HOSTS[0] != '127.0.0.1':
        host = 'http://' + ALLOWED_HOSTS[0]
    else:
        host = 'http://127.0.0.1:8000'
    # author = comment.entry_id.owner
    context = {
        'author': author,
        'current_blog': current_blog,
        'host': host,
        'current_url': current_url,
    }
    subject = render_to_string('email/new_comment_letter_subject.txt', context)
    body_text = render_to_string('email/new_comment_letter_body.txt', context)
    author.email_user(subject, body_text)
    logging.info(f"Email пользователю \'{author}\' отправлен.")


