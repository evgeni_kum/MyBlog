from django.shortcuts import render
from blogapp.forms import SearchBlog
from blogapp.models import Tematics, Blog, Entry, Comment, Answer
from cabinet.models import AdvUser
from django.core.paginator import Paginator
from .forms import SendMessage
from django.template.loader import render_to_string


def popup_and_last():
    populated = []
    arr_populated = Entry.objects.all().order_by('-count')
    count_pop = len(arr_populated)
    if count_pop <= 10:
        count_pop_max = count_pop
    else:
        count_pop_max = 10
    for i in range(count_pop_max):
        populated.append(arr_populated[i])

    last_entrys = []
    arr_populated = Entry.objects.all().order_by('-created')
    count_pop = len(arr_populated)
    if count_pop <= 10:
        count_pop_max = count_pop
    else:
        count_pop_max = 10
    for i in range(count_pop_max):
        last_entrys.append(arr_populated[i])

    return populated, last_entrys


def index(request, pk=None):
    title_name = 'Мой блог'
    description = "Мой блог: создай свой блог, делись мыслями и интересными историями."
    visible_blogs = Blog.objects.filter(visible='open').order_by('name')

    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов

    paginator = Paginator(visible_blogs, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    populated, last_entrys = popup_and_last()

    arr = []
    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
        entrys = Entry.objects.filter(blog_id=pk).count()
        """ Ниже формируем массив для вывода вспомогательных данных в шаблон,
                поскольку требуется вывод сложных структур
            """

        for item_blog in blogs:
            count_entrys = Entry.objects.filter(blog_id=item_blog.pk).count()   # считаем количество сообщений блога
            item_value = {
                'pk': item_blog.pk,  # делаем привязку к блогу
                'value': count_entrys   # указываем количество сообщений блога
            }
            arr.append(item_value)
    else:
        blogs = []

    if request.method == 'POST':
        form = SearchBlog(request.POST)
        if form.is_valid():
            keydata = form.cleaned_data['keydata']
            current_blogs = Blog.objects.filter(name__icontains=keydata).order_by('name')

            paginator = Paginator(current_blogs, 10)
            page_number = request.GET.get('page')
            page_obj = paginator.get_page(page_number)

            context = {'page_obj': page_obj,
                       'users_count': users_count,
                       'blogs_count': blogs_count,
                       'all_know': all_know,
                       'keydata': keydata,
                       'serch_word': keydata
                       }
            return render(request, 'search_blog.html', context)
        else:
            context = {'form': form, }
            return render(request, 'index.html', context)
    else:
        form = SearchBlog()

        context = {'set_title': title_name,
                   'description': description,
                   'form': form,
                   'blogs': blogs,
                   'open_blogs': visible_blogs,
                   'arr': arr,   # передаем свои дополнительные данные
                   'page_obj': page_obj,
                   'users_count': users_count,
                   'blogs_count': blogs_count,
                   'all_know': all_know,
                   'populated': populated,
                   'last_entrys': last_entrys
                   }
        return render(request, 'index.html', context)


def reload_group(request):
    area_id = request.GET.get('id_area')
    current_group = Tematics.objects.filter(area_kn=area_id).order_by('name')
    return render(request, 'inc_templates_misc/group_dropdown_list_options.html', {'group_kn': current_group})


def reload_data_left_bar(request):
    """Здесь динамически выводим актуальные данные на странице в блок слева, обновляем 1 раз в 5 сек"""
    users_count =AdvUser.objects.all().count()     # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()     # счетчик всех блогов
    all_know = Tematics.objects.all()     # все темы блогов
    comment_flag = False
    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
        arr = []
        for item_blog in blogs:
            # считаем количество сообщений блога
            count_entrys = Entry.objects.filter(blog_id=item_blog.pk).count()

            # считаем количество новых комментариев
            new_comments = Comment.objects.filter(entry_id__blog_id=item_blog.pk, new=True).count()
            if new_comments:
                comment_flag = True
            item_value = {
                'pk': item_blog.pk,  # делаем привязку к блогу
                'value': count_entrys,   # указываем количество сообщений блога
                'new_comments': new_comments,
                'visible': item_blog.visible,
            }
            arr.append(item_value)
        context = {
            'is_authenticated': True,
            'username': request.user.username,
            'blogs_count': blogs_count,
            'users_count': users_count,
            'users_blogs': Blog.objects.filter(owner=request.user).count(),
            'blogs': blogs,
            'arr': arr,
            'all_know': all_know,
            'comment_flag': comment_flag
        }
    else:
        context = {
            'is_authenticated': False,
            'blogs_count': blogs_count,
            'users_count': users_count,
            'all_know': all_know,
        }

    return render(request, 'inc_templates_misc/reload_data_left_bar.html', {'context': context})


def reload_comment_block(request, pk):
    current_comments = Comment.objects.filter(entry_id=pk).order_by('created')
    current_user = request.user
    current_entry = Entry.objects.get(pk=pk)    # получили сообщение
    current_comments_count = current_comments.count()
    array = []  # пустой массив для данных

    for comment in current_comments:
        current_item_answer = Answer.objects.filter(comment_id=comment.pk)
        item_value = {
            'pk': comment.pk,
            'value': current_item_answer
        }
        array.append(item_value)

    owner_entry = current_entry.owner
    if current_user == owner_entry:
        # обнуляем признак "new" только если смотрит хозяин сообщения
        show_new = True
        owner = True
        new_comments = Comment.objects.filter(entry_id=pk, new=True,).order_by('created')
        if new_comments:
            first_comment = new_comments[0]
            first_comment.new = False
            first_comment.save()

    else:
        show_new = False
        owner = False

    context = {
        'current_comments': current_comments,
        'current_comments_count': current_comments_count,
        'show_new': show_new,
        'owner': owner,     # хозяин / гость блога
        'array': array,
    }
    return render(request, 'inc_templates_misc/reload_comment_block.html', {'context': context})


def send_message(request, pk):
    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов
    populated, last_entrys = popup_and_last()

    if request.method == 'POST':
        send_form = SendMessage(request.POST)
        if send_form.is_valid():
            text_data = send_form.cleaned_data['text_data']
            author = AdvUser.objects.get(pk=pk)

            moderators = AdvUser.objects.filter(is_staff=True)
            for moderator in moderators:
                context = {'moderator': moderator, 'text_data': text_data, 'author': author}
                subject = render_to_string('email/new_messages_letter.txt', context)
                body_text = render_to_string('email/new_messages_body.txt', context)
                moderator.email_user(subject, body_text)
            context = {
                'title_name': 'Создание сообщения',
                'description': "Создание сообщения",
                'send_form': send_form,
                'users_count': users_count,
                'blogs_count': blogs_count,
                'all_know': all_know,
                'populated': populated,
                'last_entrys': last_entrys
            }
            return render(request, 'messages_letter_succes.html', context)
        else:
            context = {'send_form': send_form, }
            return render(request, 'index.html', context)

    else:
        send_form = SendMessage()
        context = {
            'title_name': 'Создание сообщения',
            'description': "Создание сообщения",
            'send_form': send_form,
            'users_count': users_count,
            'blogs_count': blogs_count,
            'all_know': all_know,
            'populated': populated,
            'last_entrys': last_entrys
        }
        return render(request, 'send_message.html', context)


def site_info(request):
    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
    else:
        blogs = []

    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов
    populated, last_entrys = popup_and_last()
    context = {
        'title_name': 'О сайте',
        'description': "О сайте",
        'users_count': users_count,
        'blogs_count': blogs_count,
        'all_know': all_know,
        'blogs': blogs,
        'populated': populated,
        'last_entrys': last_entrys
    }
    return render(request, 'site_info.html', context)


def contacts(request):
    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов

    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
    else:
        blogs = []

    populated, last_entrys = popup_and_last()

    context = {
        'title_name': 'Контакты',
        'description': "Контакты",
        'users_count': users_count,
        'blogs_count': blogs_count,
        'all_know': all_know,
        'blogs': blogs,
        'populated': populated,
        'last_entrys': last_entrys
    }
    return render(request, 'contacts.html', context)
