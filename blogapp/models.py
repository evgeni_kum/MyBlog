from django.utils.html import mark_safe
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from markdown import markdown

from mainapp.tasks import send_new_comment_notification
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
from PIL import Image


# Максимальный размер изображения по большей стороне
_MAX_SIZE = 700


class Tematics(models.Model):
    name = models.CharField(max_length=100, verbose_name="Тема блога")
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=8, decimal_places=0, default=500)

    class Meta:
        verbose_name_plural = "Темы блогов"
        verbose_name = "Тема блога"
        ordering = ['rating']

    def __str__(self):
        return self.name


class Blog(models.Model):
    VISIBLE = (
        ('open', 'Показывать всем'),
        ('close', 'Скрыть'),
    )
    name = models.CharField(max_length=100, verbose_name="Название")
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=8, decimal_places=0, default=500)
    group_kn = models.ForeignKey(Tematics, on_delete=models.SET_NULL, null=True, verbose_name='Тематика блога')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Автор блога")
    visible = models.CharField(max_length=5, choices=VISIBLE, blank=True, default='close',
                               verbose_name="Настройки видимости", )
    text = models.TextField(verbose_name="Краткое описание блога")
    # slug = models.SlugField(max_length=200, db_index=True, unique=True,  default=None)

    class Meta:
        verbose_name_plural = "Блоги"
        verbose_name = "Блог"
        ordering = ['rating']

    def __str__(self):
        """Возвращает строковое проедставление модели, первые 50 символов"""
        return self.text[:50] + "..."


class Entry(models.Model):
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    text = models.TextField(verbose_name="Текст сообщение")
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=8, decimal_places=0, default=500)
    blog_id = models.ForeignKey(Blog, on_delete=models.CASCADE, null=True, verbose_name='Блог')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Автор сообщения")
    count = models.PositiveIntegerField(verbose_name="Просмотры", default=0)
    # slug = models.SlugField(max_length=200, db_index=True, unique=True,  default=None)
    img = ProcessedImageField(upload_to='images',
                              null=True,
                              blank=True,)
    entry_thumbnail = ImageSpecField(source='img',
                                     processors=[ResizeToFill(100, 56)],
                                     format='JPEG',
                                     options={'quality': 60},
                                     )

    def get_text_as_markdown(self):
        return mark_safe(markdown(self.text, safe_mode='escape'))

    def save(self, *args, **kwargs):
        # Сначала - обычное сохранение
        super(Entry, self).save(*args, **kwargs)

        # Проверяем, есть ли изображение?
        if self.img:
            filepath = self.img.path
            width = self.img.width
            height = self.img.height

            max_size = max(width, height)

            # Может, и не надо ничего менять?
            if max_size > _MAX_SIZE:
                # Надо, Федя, надо
                image = Image.open(filepath)
                # resize - безопасная функция, она создаёт новый объект, а не
                # вносит изменения в исходный, поэтому так
                image = image.resize(
                    (round(width / max_size * _MAX_SIZE),  # Сохраняем пропорции
                     round(height / max_size * _MAX_SIZE)),
                    Image.ANTIALIAS
                )
                # И не забыть сохраниться
                image.save(filepath)

    class Meta:
        verbose_name_plural = "Сообщения для блогов"
        verbose_name = "Сообщение для блогов"
        ordering = ['created']

    def __str__(self):
        """Возвращает строковое проедставление модели, первые 50 символов"""
        return self.text[:50] + "..."


class BlockEntry(models.Model):
    entry_id = models.ForeignKey(Entry, on_delete=models.CASCADE, null=True, verbose_name='Запись блога')
    text = models.TextField(null=True, blank=True, verbose_name="Текст блока")
    img = models.ImageField(null=True, blank=True, verbose_name='Изображение', upload_to='images')

    def save(self, *args, **kwargs):
        # Сначала - обычное сохранение
        super(BlockEntry, self).save(*args, **kwargs)
        # Проверяем, есть ли изображение?
        if self.img:
            filepath = self.img.path
            width = self.img.width
            height = self.img.height
            max_size = max(width, height)
            # Может, и не надо ничего менять?
            if max_size > _MAX_SIZE:
                # Надо, Федя, надо
                image = Image.open(filepath)
                # resize - безопасная функция, она создаёт новый объект, а не
                # вносит изменения в исходный, поэтому так
                image = image.resize(
                    (round(width / max_size * _MAX_SIZE),  # Сохраняем пропорции
                     round(height / max_size * _MAX_SIZE)),
                    Image.ANTIALIAS
                )
                # И не забыть сохраниться
                image.save(filepath)

    class Meta:
        verbose_name_plural = "Блоки сообщений для блогов"
        verbose_name = "Блок сообщения для блога"
        # ordering = ['pk']

    def __str__(self):
        """Возвращает строковое проедставление модели, первые 50 символов"""
        return self.text[:50] + "..."


class Comment(models.Model):
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    text = models.TextField(verbose_name="Текст комментария")
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=8, decimal_places=0, default=500)
    entry_id = models.ForeignKey(Entry, on_delete=models.CASCADE, null=True, verbose_name='Сообщение')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Автор комментария")
    new = models.BooleanField(default=True, verbose_name='Новый комментарий?')

    class Meta:
        verbose_name_plural = "Комментарии"
        verbose_name = "Комментарии"
        ordering = ['created']

    def __str__(self):
        """Возвращает строковое проедставление модели, первые 50 символов"""
        return self.text[:50] + "..."


def post_save_dispatcher(sender, **kwargs):
    comment_obj = kwargs['instance']
    commnt = comment_obj.text
    author = comment_obj.entry_id.owner  # автор блога
    blog = comment_obj.entry_id.blog_id.name  # название блога
    entry_url = comment_obj.entry_id.pk  # url для entry
    if kwargs['created'] and author.send_messages:
        # send_new_comment_notification(kwargs['instance'])
        send_new_comment_notification.delay(author.pk, blog, entry_url)


post_save.connect(post_save_dispatcher, sender=Comment)


class Answer(models.Model):
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    text = models.TextField(verbose_name="Текст ответа")
    rating = models.DecimalField(verbose_name='Рейтинг', max_digits=8, decimal_places=0, default=500)
    comment_id = models.CharField(max_length=50, verbose_name='ID_комментария')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="Автор ответа")
    new = models.BooleanField(default=True, verbose_name='Новый ответ?')

    class Meta:
        verbose_name_plural = "Ответы"
        verbose_name = "Ответ"
        ordering = ['created']

    def __str__(self):
        """Возвращает строковое проедставление модели, первые 50 символов"""
        return self.text[:50] + "..."
