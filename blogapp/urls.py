from django.urls import path, re_path
from .views import *


app_name = 'blog_app'


urlpatterns = [
    path('blog_create/', BlogCreate.as_view(), name='blog_create'),
    path('blog_detail/<int:pk>/', blog_detail, name='blog_detail'),
    path('blog_update/<int:pk>/', BlogUpdate.as_view(), name='blog_update'),
    path('blog_delete/<int:pk>/', BlogDelete.as_view(), name='blog_delete'),

    path('entry_create/<int:pk>/', EntryCreate.as_view(), name='entry_create'),
    path('entry_detail/<int:pk>/', entry_detail, name='entry_detail'),
    path('entry_update/<int:pk>/', EntryUpdate.as_view(), name='entry_update'),

    path('by_tematics/<int:pk>/', BlogByTematicsListView.as_view(), name='by_tematics'),

    path('by_owner/<int:pk>/', BlogByOwnerListView.as_view(), name='by_owner'),

    path('block_entry_create/<int:pk>/', BlockEntryCreate.as_view(), name='block_entry_create'),
    path('block_update/<int:entry_pk>/<int:pk>/', BlockEntryUpdate.as_view(), name='block_update'),
    path('block_delete/<int:entry_pk>/<int:pk>/', BlockDelete.as_view(), name='block_delete'),

]
