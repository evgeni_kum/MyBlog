from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from cabinet.models import AdvUser
from mainapp.views import popup_and_last
from .models import Blog, Entry, Comment, Tematics, BlockEntry
from .forms import BlogForm, EntryForm, CommentForm, AnswerForm, BlockEntryForm
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.views import generic
from functools import wraps
from django.db.models import F
from django.db import transaction


# ---------- Blog ----------
def blog_detail(request, pk):
    current_blog = Blog.objects.get(pk=pk)
    title_name = current_blog.name

    current_entrys = Entry.objects.filter(blog_id=pk).order_by('created')

    paginator = Paginator(current_entrys, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов

    populated, last_entrys = popup_and_last()

    """ Ниже формируем массив для вывода вспомогательных данных в шаблон,
        поскольку требуется вывод сложных структур
    """
    arr = []
    for item_entry in current_entrys:
        current_comments = Comment.objects.filter(entry_id=item_entry.pk).count()
        item_value = {
            'pk': item_entry.pk,
            'value': current_comments
        }
        arr.append(item_value)

    if request.user.is_authenticated:
        current_user = request.user
    else:
        current_user = None

    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
    else:
        blogs = []

    description = 'Блог: детальный просмотр.'
    context = {
        'set_title': title_name,
        'description': description,
        'current_blog': current_blog,
        'current_entrys': current_entrys,
        'current_user': current_user,
        'arr': arr,
        'page_obj': page_obj,
        'users_count': users_count,
        'blogs_count': blogs_count,
        'all_know': all_know,
        'blogs': blogs,
        'populated': populated,
        'last_entrys': last_entrys
    }
    return render(request, 'blog_detail.html', context)


class BlogCreate(LoginRequiredMixin, CreateView):
    model = Blog
    template_name = 'create_dlog.html'
    form_class = BlogForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Создание нового блога'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.kwargs.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_success_url(self):
        return reverse_lazy('blog_app:blog_detail', args=(self.object.id,))

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(BlogCreate, self).form_valid(form)


class BlogUpdate(LoginRequiredMixin, UpdateView):
    fields = ['name', 'group_kn', 'visible', 'text']
    success_url = reverse_lazy('visitor')
    template_name = 'create_dlog.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Изменить данные блога'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_queryset(self):
        return Blog.objects.all()

    def get_success_url(self):
        return reverse_lazy('blog_app:blog_detail', args=(self.object.id,))


class BlogDelete(LoginRequiredMixin, DeleteView):
    model = Blog
    success_url = reverse_lazy('index')
    template_name = 'blog_confirm_delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Удаление блога!'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context


# ---------- Entry ----------
class EntryCreate(LoginRequiredMixin, CreateView):
    model = Entry
    template_name = 'create_entry.html'
    form_class = EntryForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Создание сообщения блога'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_success_url(self):
        return reverse_lazy('blog_app:blog_detail', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.instance.blog_id = Blog.objects.get(pk=self.kwargs.get('pk'))
        return super(EntryCreate, self).form_valid(form)


class EntryUpdate(LoginRequiredMixin, UpdateView):
    fields = ('img', 'text')
    template_name = 'create_entry.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Редактирование записи блога'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_queryset(self):
        return Entry.objects.all()

    def get_success_url(self):
        return reverse_lazy('blog_app:entry_detail', kwargs={'pk': self.kwargs.get('pk')})


def counted(f):
    @wraps(f)
    def decorator(request, pk, *args, **kwargs):
        with transaction.atomic():
            counter = Entry.objects.get(pk=pk)
            counter.count = F('count') + 1
            counter.save()
        return f(request, pk, *args, **kwargs)
    return decorator


@counted
def entry_detail(request, pk):

    current_entry = Entry.objects.get(pk=pk)

    current_comments = Comment.objects.filter(entry_id=pk)
    current_user = request.user
    users_count = AdvUser.objects.all().count()  # счетчик всех пользователей
    blogs_count = Blog.objects.all().count()  # счетчик всех блогов
    all_know = Tematics.objects.all()  # все темы блогов

    current_blog = Blog.objects.get(pk=current_entry.blog_id.pk)
    title_name = current_blog.name

    current_entry = Entry.objects.get(pk=pk)  # получили сообщение
    owner_entry = current_entry.owner

    populated, last_entrys = popup_and_last()

    """
    Если user на странице - хозяин блога, то ему форма Answer;
    Если нет, то ему форма Comment  
    """
    if request.user != owner_entry:
        initial = {'owner': current_user.pk, 'entry_id': pk}
        form = CommentForm(initial=initial)
        owner = False
    else:
        initial = {'owner': current_user.pk}
        form = AnswerForm(initial=initial)
        owner = True

    if request.method == 'POST':
        if request.user != owner_entry:
            form = CommentForm(request.POST)
            if form.is_valid():
                form.save()
                messages.add_message(request, messages.SUCCESS, 'Комментарий добавлен')
            else:
                form = CommentForm()
                messages.add_message(request, messages.WARNING, 'Комментарий не добавлен')
        else:
            form = AnswerForm(request.POST)
            if form.is_valid():
                form.save()
                messages.add_message(request, messages.SUCCESS, 'Ответ на комментарий добавлен')
            else:
                form = AnswerForm()
                messages.add_message(request, messages.WARNING, 'Ответ на комментарий не добавлен')

    description = 'Блог: детальный просмотр.'
    if request.user.is_authenticated:
        blogs = Blog.objects.filter(owner=request.user).order_by('name')
    else:
        blogs = []

    context = {
        'set_title': title_name,
        'current_user': current_user,
        'description': description,
        'current_comments': current_comments,
        'current_entry': current_entry,
        'form': form,
        'owner': owner,
        'users_count': users_count,
        'blogs_count': blogs_count,
        'all_know': all_know,
        'all_block': BlockEntry.objects.filter(entry_id=pk),
        'blogs': blogs,
        'populated': populated,
        'last_entrys': last_entrys
    }

    return render(request, 'entry_detail.html', context)


# ---------- Вывод по фильтрам ----------

class BlogByTematicsListView(generic.ListView):
    """=== фильтр по теме блога ==="""
    model = Blog
    paginate_by = 20
    template_name = 'blog_list.html'

    def get_queryset(self):
        return Blog.objects.filter(group_kn=self.kwargs.get('pk'), visible='open').order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['description'] = 'Блог: фильтр по темам'
        context['current_tematics'] = Tematics.objects.get(pk=self.kwargs.get('pk'))
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        try:
            context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        except:
            context['blogs'] = []
        return context


class BlogByOwnerListView(generic.ListView):
    """=== фильтр по автору блога ==="""
    model = Blog
    paginate_by = 20
    template_name = 'blog_list_by_owner.html'

    def get_queryset(self):
        return Blog.objects.filter(owner=self.kwargs.get('pk'), visible='open').order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['description'] = 'Фильтр блогов по автору'
        context['current_owner'] = AdvUser.objects.get(pk=self.kwargs.get('pk'))
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        try:
            context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        except:
            context['blogs'] = []
        return context


# ---------- BlockEntry ----------
class BlockEntryCreate(LoginRequiredMixin, CreateView):
    model = BlockEntry
    template_name = 'create_block_entry.html'
    fields = ('img', 'text',)
    # form_class = BlockEntryForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Создание блока'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_queryset(self):
        return Entry.objects.get(pk=self.kwargs.get('pk'))

    def get_success_url(self):
        return reverse_lazy('blog_app:entry_detail', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        form.instance.entry_id = Entry.objects.get(pk=self.kwargs.get('pk'))
        return super(BlockEntryCreate, self).form_valid(form)


class BlockEntryUpdate(LoginRequiredMixin, UpdateView):
    model = BlockEntry
    fields = ('img', 'text')
    template_name = 'create_block_entry.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Редактирование записи блока'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_success_url(self):
        return reverse_lazy('blog_app:entry_detail', kwargs={'pk': self.kwargs.get('entry_pk')})


class BlockDelete(LoginRequiredMixin, DeleteView):
    model = BlockEntry
    template_name = 'blog_confirm_delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['set_title'] = 'Удаление блока записи!'
        context['users_count'] = AdvUser.objects.all().count()  # счетчик всех пользователей
        context['blogs_count'] = Blog.objects.all().count()  # счетчик всех блогов
        context['all_know'] = Tematics.objects.all()  # все темы блогов
        context['set_message'] = 'запись блога'
        context['blogs'] = Blog.objects.filter(owner=self.request.user).order_by('name')
        populated, last_entrys = popup_and_last()
        context['populated'] = populated
        context['last_entrys'] = last_entrys
        return context

    def get_success_url(self):
        return reverse_lazy('blog_app:entry_detail', kwargs={'pk': self.kwargs.get('entry_pk')})