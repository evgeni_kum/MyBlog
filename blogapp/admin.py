from django.contrib import admin
from .models import Tematics, Blog, Entry, Comment, Answer


class TematicsAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating']
    list_filter = ['name', 'rating']
    list_editable = ['rating']


admin.site.register(Tematics, TematicsAdmin)


class BlogAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating', 'group_kn', 'created', 'owner', 'visible', 'text']
    list_filter = ['name', 'rating', 'group_kn', 'created', 'owner', 'visible']
    list_editable = ['group_kn', 'rating', 'visible']


admin.site.register(Blog, BlogAdmin)


class EntryAdmin(admin.ModelAdmin):
    list_display = ['blog_id', 'owner', ]
    list_filter = ['blog_id', 'owner']


admin.site.register(Entry, EntryAdmin)


class CommentAdmin(admin.ModelAdmin):
    list_display = ['entry_id', 'owner', ]
    list_filter = ['entry_id', 'owner']


admin.site.register(Comment, CommentAdmin)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ['comment_id', 'owner', ]
    list_filter = ['comment_id', 'owner']


admin.site.register(Answer, AnswerAdmin)
