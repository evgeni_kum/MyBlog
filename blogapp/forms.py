from django import forms
from django.core.exceptions import ValidationError

from .models import Tematics, Blog, Entry, Comment, Answer, BlockEntry


class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('name', 'group_kn', 'visible', 'text')
    """
    def clean_slug(self):
        new_slug = self.changed_data['slug'].lower()

        if new_slug == 'create':
            raise ValidationError('Slug may not be "Create"')
        if Blog.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError('Slug mast be unique')
    """

class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ('img', 'text',)
        widgets = {'owner': forms.HiddenInput, 'blog_id': forms.HiddenInput, 'rating': forms.HiddenInput}


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', 'owner', 'entry_id')
        widgets = {'owner': forms.HiddenInput, 'entry_id': forms.HiddenInput, 'rating': forms.HiddenInput}


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('text', 'owner', 'comment_id')
        widgets = {'owner': forms.HiddenInput, 'comment_id': forms.HiddenInput, 'rating': forms.HiddenInput}


class SearchBlog(forms.Form):
    keydata = forms.CharField(label='Введите название, или часть названия')


class BlockEntryForm(forms.ModelForm):
    class Meta:
        model = BlockEntry
        fields = ('img', 'text', 'entry_id')
        # widgets = {'entry_id': forms.HiddenInput, }
