from django.template.loader import render_to_string
from django.core.signing import Signer

from blogsite.settings import ALLOWED_HOSTS

signer = Signer()


def send_new_comment_notification(pk):
    comment = Comment.objects.get(pk=pk)
    if ALLOWED_HOSTS:
        host = 'http://' + ALLOWED_HOSTS[0]
    else:
        host = 'http://localhost:8000'
    author = comment.entry_id.owner
    context = {'author': author, 'host': host, 'comment': comment}
    subject = render_to_string('email/new_comment_letter_subject.txt', context)
    body_text = render_to_string('email/new_comment_letter_body.txt', context)
    author.email_user(subject, body_text)
