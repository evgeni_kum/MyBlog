# MyBlog

Базовые:

python3 -m venv env  виртуальное окружение

source env/bin/activate - активируем

pip install django
pip install django-bootstrap4



для работы на сервере понадобятся модули:

pip install gunicorn psycopg2-binary

--- Формат и константы файла конфига ---
--- все значения без кавычек! ---
[main]
SECRET_KEY: ****
Debug: True

[databases]
ENGINE: django.db.backends.sqlite3
NAME: os.path.join(BASE_DIR, 'db.sqlite3')

[google]
RECAPTCHA_SECRET_KEY: ****

[email]
EMAIL_HOST: mtp.yandex.ru
EMAIL_PORT: 465
EMAIL_HOST_USER: ****
DEFAULT_FROM_EMAIL: ****
EMAIL_HOST_PASSWORD: ****
EMAIL_USE_SSL: True
EMAIL_USE_TLS: False


==Текущие задачи==

-Стили страницы "entry-detail"
-Email о появлении новых комментариев, ответов(?)
-Пагинация
-фильтрация по темам в блоке слева
